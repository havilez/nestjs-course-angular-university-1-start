import { Connection } from 'mongoose';
import { LessonSchema } from '../schemas/lessons.schema';

export  const  LessonProviders = [
   {
    provide: 'LESSON_MODEL',
    useFactory: ( connection: Connection ) => connection.model('Lesson', LessonSchema),
    inject: ['DATABASE_CONNECTION'],
   },
];
