import { Connection } from 'mongoose';
import { CourseSchema } from '../schemas/courses.schema';

export  const  CourseProviders = [
   {
    provide: 'COURSE_MODEL',
    useFactory: ( connection: Connection ) => connection.model('Course', CourseSchema),
    inject: ['DATABASE_CONNECTION'],
   },
];
