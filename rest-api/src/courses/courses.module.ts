import { Module } from '@nestjs/common';
import { CoursesController } from './courses.controller';
import { DatabaseModule } from '../database/database.module';
import { CourseProviders } from './providers/course.providers'
import { LessonProviders } from './providers/lesson.providers';
import { CoursesRepository } from './repositories/courses.repository'

@Module({
  imports: [
    DatabaseModule,
  ],
  providers: [ ...CourseProviders, ...LessonProviders, CoursesRepository ],
  controllers: [CoursesController],

})
export class CoursesModule {
 
}
