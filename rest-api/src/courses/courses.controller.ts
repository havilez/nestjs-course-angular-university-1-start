import { Controller, Get, Put, Param, Body, Delete, Post, HttpException, UseFilters, ParseIntPipe } from '@nestjs/common';
import { Course } from '../../shared/course';
import { CoursesRepository } from './repositories/courses.repository';
import { HttpExceptionFilter } from '../filters/http.filter';
import { from } from 'rxjs';
import { ToIntegerPipe } from '../pipes/to-integer.pipe';

@Controller('courses')
export class CoursesController {

   constructor( private coursesDB: CoursesRepository) {

   }

   @Get()
   async findAllCourses(): Promise<Course[] | any[]> {
      console.log( 'retrieving all courses');
      return  this.coursesDB.findAll();
   }

   @Put(':courseId')
   // make exception filter global instead
   // @UseFilters(new HttpExceptionFilter())
   async updateCourse(@Param('courseId') courseId: string,
                      @Body('seqNo', ParseIntPipe) seqNo: number,
                      @Body() changes: Partial<Course> ): Promise<Course> {
      console.log('updating course');

      console.log(`seqNo value  ${seqNo}, type:  ${typeof seqNo}` );

      // _id field not allowed on Partial<Course>
      // @ts-ignore
      if ( changes._id ) {
         console.log('execption occurred!!!');
         throw new HttpException(`Can\'t update course id`, 400);
      }

      return this.coursesDB.updateCourse(courseId, changes );
   }

   @Delete(':courseId')
   async deleteCourse(@Param('coursseId') courseId: string) {
      console.log('deleting course');

      return this.coursesDB.deleteCourse( courseId );
   }

   @Post()
   async createCourse(@Body() course: Partial<Course>): Promise<Course> {
      console.log('creating course');

      return this.coursesDB.addCourse( course );
   }
}
